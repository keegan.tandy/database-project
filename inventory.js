




function createInventoryTables() {
    fetch('/GetInventory', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => response.json())
        .then(data => {
            const table = document.getElementById('dataTable');
            const tbody = document.createElement('tbody');

            // Create table header row
            const headerRow = document.createElement('tr');
            Object.keys(data[0]).forEach(key => {
                const th = document.createElement('th');
                th.textContent = key;
                headerRow.appendChild(th);
            });
            // Add two more th elements for Edit and Delete buttons
            const editTh = document.createElement('th');
            editTh.textContent = 'Edit';
            headerRow.appendChild(editTh);
            const deleteTh = document.createElement('th');
            deleteTh.textContent = 'Delete';
            headerRow.appendChild(deleteTh);

            table.appendChild(headerRow);

            // Loop through each item in the data array
            data.forEach(item => {
                // Create a new row for each item
                const row = document.createElement('tr');

                // Loop through each property of the item object
                Object.entries(item).forEach(([key, value]) => {
                    // Create a cell for each property value
                    const cell = document.createElement('td');
                    cell.textContent = value;
                    if (value === 0) {
                        cell.style.backgroundColor = 'red';
                    }
                    row.appendChild(cell);
                });

                // Add Edit button to each row
                const editCell = document.createElement('td');
                const editButton = document.createElement('button');
                editButton.textContent = 'Edit';
                // Add Delete button to each row
                const deleteCell = document.createElement('td');
                const deleteButton = document.createElement('button');
                deleteButton.textContent = 'Delete';
                deleteButton.classList.add('delete-button');

                // Add event listener for edit button
                editButton.addEventListener('click', editModeHandler);
                editButton.classList.add('fill-button');
                editCell.appendChild(editButton);
                row.appendChild(editCell);



                deleteButton.addEventListener('click', () => {
                    // Make AJAX request to delete endpoint
                    fetch('/DeleteItem', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(item) // Send the item data to the server
                    })
                        .then(response => {
                            if (response.ok) {
                                // Reload the page after successful deletion
                                window.location.reload();
                            } else {
                                console.error('Error deleting item:', response.statusText);
                            }                        })
                        .catch(error => {
                            console.error('Error deleting item:', error);
                        });
                });
                deleteCell.appendChild(deleteButton);
                row.appendChild(deleteCell);

                // Append the row to the table body
                tbody.appendChild(row);
            });

            // Append the table body to the table
            table.appendChild(tbody);
        })
        .catch(error => console.error('Error fetching inventory data:', error));
}





// Add event listener to the add row button
const addRowButton = document.getElementById('addRowButton');
addRowButton.addEventListener('click', createNewRow);

// Function to create a new row
function createNewRow() {
// Show the custom dialog box
    document.getElementById('colorDialog').style.display = 'block';

    // When the user clicks on the close button or outside the dialog, close it
    document.addEventListener('click', function(event) {
        const dialog = document.getElementById('colorDialog');
        if (event.target === dialog || event.target.classList.contains('close')) {
            dialog.style.display = 'none';
        }
    });

// When the user submits the color
    document.getElementById('colorSubmit').addEventListener('click', function() {
        const selectedColor = document.getElementById('colorInput').value;
        const selectedPrice = document.getElementById('priceInput').value;
        const selectedCost = document.getElementById('costInput').value;
        const selectedInventory = document.getElementById('inventoryInput').value;

        if (!selectedColor || !selectedPrice || !selectedCost || !selectedInventory) {
            alert('Missing field item.');
            return;
        }

        const requestData = {
            color: selectedColor,
            salePrice: selectedPrice,
            prodCost: selectedCost,
            inventory: selectedInventory
            // Add other data properties as needed
        };

        fetch('/InsertInventory', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(requestData) // Send the updated item data to the server
        })
            .then(response => {
                if (response.ok) {
                    window.location.reload();
                } else {
                    console.error('Error updating item:', response.statusText);
                }
            })
            .catch(error => {
                console.error('Error updating item:', error);
            });

    });


}


// Function to handle the 'Save' button click event
function saveModeHandler() {
    const row = this.parentNode.parentNode; // Get the row of the clicked button
    const cells = row.querySelectorAll('td');
    const updatedItem = {};
    cells.forEach((cell, index) => {
        console.log(cell);
        if (index < cells.length - 2 && index !== 0) { // Exclude the last cell containing buttons
            const input = cell.querySelector('input');
            console.log(input.value);
            updatedItem[index] = input.value;
            cell.textContent = input.value;
        }
    });

    updatedItem[0] = row.querySelector('td').textContent;

    // Make AJAX request to update endpoint
    fetch('/UpdateInventory', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(updatedItem) // Send the updated item data to the server
    })
        .then(response => {
            if (response.ok) {
                // Change button text back to 'Edit'
                const editButton = row.querySelector('button');
                editButton.textContent = 'Edit';
                // Change event listener back to handle editing
                editButton.removeEventListener('click', saveModeHandler);
                editButton.addEventListener('click', editModeHandler);
                editButton.classList.add('fill-button');
                window.location.reload();
            } else {
                console.error('Error updating item:', response.statusText);
            }
        })
        .catch(error => {
            console.error('Error updating item:', error);
        });
}

// Function to handle the 'Edit' button click event
function editModeHandler() {
    // Toggle edit mode
    const row = this.parentNode.parentNode; // Get the row of the clicked button
    const cells = row.querySelectorAll('td');
    cells.forEach(cell => {
        if (cell !== this.parentNode && cell.textContent !== "Delete" && cell.cellIndex !== 0) {
            const input = document.createElement('input');
            input.type = 'text';
            input.value = cell.textContent;
            cell.textContent = '';
            cell.appendChild(input);
        }
    });
    // Change button text to 'Save'
    this.textContent = 'Save';
    // Change event listener to handle saving
    this.removeEventListener('click', editModeHandler);
    this.addEventListener('click', saveModeHandler);
    this.classList.add('fill-button');
}



window.onload = createInventoryTables;
