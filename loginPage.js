document.getElementById('login-form').addEventListener('submit', async (event) => {
    event.preventDefault();
    const formData = new FormData(event.target);
    const email = formData.get('email');
    const password = formData.get('password');
    console.log(email+password);

    try {
        fetch('/loginQuery', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email, password })
        })
            .then(response => {
                console.log(response);
                if (response.status === 401) {
                    alert("Login invalid, Please try again");
                } else if (!response.ok) {
                    throw new Error('Failed to log in: ' + response.statusText);
                } else {
                    console.log("URL: " + response.url)
                    window.location.href = response.url;
                }
            })
            .catch(error => {
                console.error('Error:', error);
            });
    } catch (error) {
        console.error('Error:', error);
    }


});

