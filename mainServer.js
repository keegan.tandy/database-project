// noinspection DuplicatedCode
const express = require('express');
const mysql = require('mysql2');
const PORT = process.env.PORT || 8080;
var app = express();
const bodyParser = require('body-parser');

app.use(express.static(__dirname));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const dbConfig = {
    host: 'localhost',
    user: 'root',
    password: 'keegantandy572',
    database: 'danni'
};

let routes = require("./routing");

const con = mysql.createConnection(dbConfig);

app.get("/", routes.login);
app.get("/login", routes.login);

app.get("/inventory", routes.inventory);

app.get("/dashboard", routes.dashboard);

app.get("/orders", routes.orders);

app.get("/supplies", routes.supplies);

app.get("/signup", routes.signup);

app.post('/UpdateSupply', (req, res) => {

    console.log("/UpdateSupply: " + req.body[0]);

    con.connect(function (err) {
        if (err) {
            console.error(err);
            res.status(500).send('INTERNAL SERVER ERROR');
            return;
        }
        console.log(req.body.color);
        con.query("UPDATE supplies SET supply=?, cost=?, inventory=? WHERE supply = ?", [req.body[0], req.body[1], req.body[2], req.body[0]], function (err) {
            if (err) {
                console.error(err);
                res.status(500).send('Internal Server Error');
                return;
            }
            res.status(200).send('Item deleted successfully'); // Send a success message
        });
    });
});

app.post('/UpdateInventory', (req, res) => {

    console.log("/UpdateInventory: " + req.body[0]);

    con.connect(function (err) {
        if (err) {
            console.error(err);
            res.status(500).send('INTERNAL SERVER ERROR');
            return;
        }
        console.log(req.body.color);
        con.query("UPDATE product SET color=?, salePrice=?, productionCost=?, inventory=? WHERE color=?", [req.body[0], req.body[1], req.body[2], req.body[3], req.body[0]], function (err) {
            if (err) {
                console.error(err);
                res.status(500).send('Internal Server Error');
                return;
            }
            res.status(200).send('Item deleted successfully'); // Send a success message
        });
    });
});

app.post('/InsertInventory', (req, res) => {

    console.log("/InsertInventory: " + req.body);

    con.connect(function (err) {
        if (err) {
            console.error(err);
            res.status(500).send('INTERNAL SERVER ERROR');
            return;
        }
        console.log(req.body.color);
        con.query("INSERT INTO product (color, salePrice, productionCost, inventory) VALUES(?, ?, ?, ?)", [req.body.color, req.body.salePrice, req.body.prodCost, req.body.inventory], function (err) {
            if (err) {
                console.error(err);
                res.status(500).send('Internal Server Error');
                return;
            }
            res.status(200).send('Item deleted successfully'); // Send a success message
        });
    });
});



app.post('/InsertSupply', (req, res) => {

    console.log("/InsertSupply: " + req.body);

    con.connect(function (err) {
        if (err) {
            console.error(err);
            res.status(500).send('INTERNAL SERVER ERROR');
            return;
        }
        console.log(req.body.color);
        con.query("INSERT INTO supplies (supply, cost, inventory) VALUES(?, ?, ?)", [req.body.color, req.body.prodCost, req.body.inventory], function (err) {
            if (err) {
                console.error(err);
                res.status(500).send('Internal Server Error');
                return;
            }
            res.status(200).send('Item deleted successfully'); // Send a success message
        });
    });
});


app.post('/DeleteItem', (req, res) => {

    console.log(req.body);

    con.connect(function (err) {
        if (err) {
            console.error(err);
            res.status(500).send('INTERNAL SERVER ERROR');
            return;
        }
        console.log(req.body.color);
        con.query("DELETE FROM product WHERE color = ?", [req.body.color], function (err) {
            if (err) {
                console.error(err);
                res.status(500).send('Internal Server Error');
                return;
            }
            res.status(200).send('Item deleted successfully'); // Send a success message
        });
    });
});

app.post('/DeleteOrder', (req, res) => {
    console.log(req.body);

    con.connect(function (err) {
        if (err) {
            console.error(err);
            res.status(500).send('INTERNAL SERVER ERROR');
            return;
        }
        console.log(req.body.color);
        con.query("DELETE FROM orders WHERE orderNo = ?", [req.body.orderNo], function (err) {
            if (err) {
                console.error(err);
                res.status(500).send('Internal Server Error');
                return;
            }
            res.status(200).send('Item deleted successfully'); // Send a success message
        });
    });
});

app.post('/DeleteFill', (req, res) => {
    console.log(req.body);

    con.connect(function (err) {
        if (err) {
            console.error(err);
            res.status(500).send('INTERNAL SERVER ERROR');
            return;
        }
        console.log(req.body.color);
        con.query("DELETE FROM filledOrders WHERE orderNo = ?", [req.body.orderNo], function (err) {
            if (err) {
                console.error(err);
                res.status(500).send('Internal Server Error');
                return;
            }
            res.status(200).send('Item deleted successfully'); // Send a success message
        });
    });
});

app.post('/DeleteSupply', (req, res) => {

    console.log(req.body);

    con.connect(function (err) {
        if (err) {
            console.error(err);
            res.status(500).send('INTERNAL SERVER ERROR');
            return;
        }
        console.log(req.body.color);
        con.query("DELETE FROM supplies WHERE supply = ?", [req.body.supply], function (err) {
            if (err) {
                console.error(err);
                res.status(500).send('Internal Server Error');
                return;
            }
            res.status(200).send('Item deleted successfully'); // Send a success message
        });
    });
});


app.post('/FillOrder', (req, res) => {
    let today = new Date().toISOString().slice(0, 10);
    let orderDate = req.body.orderDate.slice(0,10);

    console.log(today);
    console.log(orderDate);
    con.connect(function (err) {
        if (err) {
            console.error(err);
            res.status(500).send('Internal Server Error');
            return;
        }
        con.query("INSERT INTO filledOrders (orderNo, nItems, state, orderTotal, shippingCost, shipped, dateShipped, orderDate) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
            [req.body.orderNo, req.body.nItems, req.body.state, req.body.orderTotal, req.body.shippingCost, true, today, orderDate], function (err) {
                if (err) {
                    console.error(err);
                    res.status(500).send('Internal Server Error');

                } else {
                    res.status(200).send('Item deleted successfully'); // Send a success message
                }
            });

    });
});


app.post('/GetSupplies', (req, res) => {
    con.connect(function(err) {
        if (err) {
            console.error(err);
            res.status(500).send('INTERNAL SERVER ERROR');
            return;
        }
        con.query("SELECT * FROM supplies", function (err, result) {
            if (err) {
                console.error(err);
                res.status(500).send('Internal Server Error');
                return;
            }
            if (result.length === 0) {
                // No data found
                res.status(404).send('No data found');
                return;
            }
            console.log('Data found: ', result);
            // Send the data back to the client as JSON
            res.status(200);
            res.json(result);
        });
    })
})

app.post('/GetFilled', (req, res) => {
    con.connect(function(err) {
        if (err) {
            console.error(err);
            res.status(500).send('INTERNAL SERVER ERROR');
            return;
        }
        con.query("SELECT * FROM filledOrders", function (err, result) {
            if (err) {
                console.error(err);
                res.status(500).send('Internal Server Error');
                return;
            }
            if (result.length === 0) {
                // No data found
                res.status(404).send('No data found');
                return;
            }
            console.log('Data found: ', result);
            // Send the data back to the client as JSON
            res.status(200);
            res.json(result);
        });
    })
});



app.post('/GetOrders', (req, res) => {
    con.connect(function(err) {
        if (err) {
            console.error(err);
            res.status(500).send('INTERNAL SERVER ERROR');
            return;
        }
        con.query("SELECT * FROM orders", function (err, result) {
            if (err) {
                console.error(err);
                res.status(500).send('Internal Server Error');
                return;
            }
            if (result.length === 0) {
                // No data found
                res.status(404).send('No data found');
                return;
            }
            console.log('Data found: ', result);
            // Send the data back to the client as JSON
            res.status(200);
            res.json(result);
        });
    })
})
app.post('/GetInventory', (req, res) => {
    con.connect(function(err) {
        if (err) {
            console.error(err);
            res.status(500).send('INTERNAL SERVER ERROR');
            return;
        }
        con.query("SELECT * FROM product", function (err, result) {
            if (err) {
                console.error(err);
                res.status(500).send('Internal Server Error');
                return;
            }
            if (result.length === 0) {
                // No data found
                res.status(404).send('No data found');
                return;
            }
            console.log('Data found: ', result);
            // Send the data back to the client as JSON
            res.status(200);
            res.json(result);
        });
    })
})



app.post('/signUpUser', (req, res) => {
    const { email, password} = req.body; // Destructure username, password, and email from request body

    con.connect(function (err) {
        if (err) {
            console.error(err);
            res.status(500).send('Internal Server Error');
            return;
        }
        con.query("INSERT INTO users (email, password) VALUES (?, ?)", [email, password], function (err) {
            if (err) {
                console.error(err);
                res.status(500).send('Internal Server Error');

            } else {
                res.redirect('/login');
            }
        });
    });
});




// Login endpoint
app.post('/loginQuery', (req, res) => {
    const { email, password } = req.body; // Destructure username and password from request body


    con.connect(function(err) {
        if (err) {
            console.error(err);
            res.status(500).send('Internal Server Error');
            return;
        }
        console.log("Officially connected and now querying the database");
        con.query("SELECT * FROM users WHERE email = ?", email, function (err, result) {
            if (err) {
                console.error(err);
                res.status(500).send('Internal Server Error');
                return;
            }
            if (result.length === 0) {
                // User not found
                res.status(401).send('User not found');
                return;
            }
            console.log("Queried the database with query and got result: " + result);

            const user = result[0]; // Assuming username is unique, so we take the first result

            if (password !== user.password) {
                res.status(401).send('Invalid username or password');
            }
            else {
                res.redirect('/dashboard');
            }
        });
    });
});

// Start server
app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});
