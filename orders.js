function createOrderTables() {
    fetch('/GetOrders', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => response.json())
        .then(data => {
            const table = document.getElementById('dataTable');
            const tbody = document.createElement('tbody');

            // Create table header row
            const headerRow = document.createElement('tr');
            Object.keys(data[0]).forEach(key => {
                const th = document.createElement('th');
                th.textContent = key;
                headerRow.appendChild(th);
            });
            // Add two more th elements for Edit and Delete buttons
            const fillTh = document.createElement('th');
            fillTh.textContent = 'Fill';
            headerRow.appendChild(fillTh);
            const deleteTh = document.createElement('th');
            deleteTh.textContent = 'Delete';
            headerRow.appendChild(deleteTh);

            table.appendChild(headerRow);

            // Loop through each item in the data array
            data.forEach(item => {
                // Create a new row for each item
                const row = document.createElement('tr');

                // Loop through each property of the item object
                Object.values(item).forEach(value => {
                    // Create a cell for each property value
                    const cell = document.createElement('td');
                    cell.textContent = value;
                    row.appendChild(cell);
                });

                // Add Delete button to each row
                const fillCell = document.createElement('td');
                const fillButton = document.createElement('button');
                fillButton.textContent = 'FullFill';
                fillButton.classList.add('fill-button');

                fillButton.addEventListener('click', () => {
                    // Make AJAX request to delete endpoint
                    fetch('/FillOrder', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(item) // Send the item data to the server
                    })
                        .then(response => {
                            if (response.ok) {
                                // Reload the page after successful deletion
                                window.location.reload();
                            } else {
                                console.error('Error deleting item:', response.statusText);
                            }                        })
                        .catch(error => {
                            console.error('Error deleting item:', error);
                        });

                    fetch('/DeleteOrder', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(item) // Send the item data to the server
                    })
                        .then(response => {
                            if (response.ok) {
                                // Reload the page after successful deletion
                                window.location.reload();
                            } else {
                                console.error('Error deleting item:', response.statusText);
                            }                        })
                        .catch(error => {
                            console.error('Error deleting item:', error);
                        });
                });
                fillCell.appendChild(fillButton);
                row.appendChild(fillCell);

                // Add Delete button to each row
                const deleteCell = document.createElement('td');
                const deleteButton = document.createElement('button');
                deleteButton.textContent = 'Delete';
                deleteButton.classList.add('delete-button');

                deleteButton.addEventListener('click', () => {
                    // Make AJAX request to delete endpoint
                    fetch('/DeleteOrder', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(item) // Send the item data to the server
                    })
                        .then(response => {
                            if (response.ok) {
                                // Reload the page after successful deletion
                                window.location.reload();
                            } else {
                                console.error('Error deleting item:', response.statusText);
                            }                        })
                        .catch(error => {
                            console.error('Error deleting item:', error);
                        });
                });
                deleteCell.appendChild(deleteButton);
                row.appendChild(deleteCell);

                // Append the row to the table body
                tbody.appendChild(row);
            });




            // Append the table body to the table
            table.appendChild(tbody);
        })
        .catch(error => console.error('Error fetching inventory data:', error));
}



function createFillTables(){
    fetch('/GetFilled', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => response.json())
        .then(data => {
            const table = document.getElementById('fillTable');
            const tbody = document.createElement('tbody');

            // Create table header row
            const headerRow = document.createElement('tr');
            Object.keys(data[0]).forEach(key => {
                const th = document.createElement('th');
                th.textContent = key;
                headerRow.appendChild(th);
            });

            const deleteTh = document.createElement('th');
            deleteTh.textContent = 'Delete';
            headerRow.appendChild(deleteTh);

            table.appendChild(headerRow);

            // Loop through each item in the data array
            data.forEach(item => {
                // Create a new row for each item
                const row = document.createElement('tr');

                // Loop through each property of the item object
                Object.values(item).forEach(value => {
                    // Create a cell for each property value
                    const cell = document.createElement('td');
                    cell.textContent = value;
                    row.appendChild(cell);
                });

                // Add Delete button to each row
                const deleteCell = document.createElement('td');
                const deleteButton = document.createElement('button');
                deleteButton.textContent = 'Delete';
                deleteButton.classList.add('delete-button');

                deleteButton.addEventListener('click', () => {
                    // Make AJAX request to delete endpoint
                    fetch('/DeleteFill', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(item) // Send the item data to the server
                    })
                        .then(response => {
                            if (response.ok) {
                                // Reload the page after successful deletion
                                window.location.reload();
                            } else {
                                console.error('Error deleting item:', response.statusText);
                            }                        })
                        .catch(error => {
                            console.error('Error deleting item:', error);
                        });
                });
                deleteCell.appendChild(deleteButton);
                row.appendChild(deleteCell);

                // Append the row to the table body
                tbody.appendChild(row);
            });
            // Append the table body to the table
            table.appendChild(tbody);
        })
        .catch(error => console.error('Error fetching inventory data:', error));
}




window.onload = function() {
    createOrderTables();
    createFillTables();
};
