let fs = require('fs');

function load(path, res){
    console.log(path);
    try{
        let content = fs.readFileSync(path);
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(content);

    }catch(err){
        //data dump
        console.log(err);
    }
    res.end();
}

function login(req, res){
    console.log('Navigating to login page');
    load("./login.html",res);
}

function dashboard(req, res){
    console.log('Navigating to dashboard');
    load("./dashboard.html", res);
}

function inventory(req, res) {
    console.log('Navigating to Inventory');
    load("./inventory.html", res);
}

function orders(req, res) {
    console.log('Navigating to Orders');
    load("./orders.html", res);
}

function supplies(req, res) {
    console.log('Navigating to Supplies');
    load("./supplies.html", res);
}

function signup(req, res) {
    console.log('Navigating to Signup');
    load("./signup.html", res);
}


module.exports = {login, dashboard, inventory, orders, supplies, signup};