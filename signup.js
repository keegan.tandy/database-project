document.getElementById('login-form').addEventListener('submit', async (event) => {
    event.preventDefault();
    const formData = new FormData(event.target);
    const email = formData.get('email');
    const password = formData.get('password');
    console.log(email+password);

    try {
        fetch('/signUpUser', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({email, password})
        })
            .then(response => {
                console.log(response);
                if (response.status === 401) {
                    alert("The information given is already in use. Please try again");
                } else if (!response.ok) {
                    throw new Error('Failed to sign up: ' + response.statusText);
                } else {
                    console.log("URL: " + response.url)
                    window.location.href = response.url;
                }
            })
            .catch(error => {
                console.error('Error:', error);
            });
    } catch (error) {
        console.error('Error:', error);
    }


});